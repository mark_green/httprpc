package httpRpc

import (
	"net/http"
	"encoding/json"
	"reflect"
)

func HandleHttpGet(rpcDelegate interface{}) func(resp http.ResponseWriter, req *http.Request) {
	delegateType := reflect.TypeOf(rpcDelegate)
	if delegateType.NumIn() != 0 {
		panic("HandleHttpRpc: Delegate should have no inputs")
	}
	if delegateType.NumOut() != 1 {
		panic("HandleHttpRpc: Delegate should have exactly one output")
	}
	if delegateType.Out(0).Kind() != reflect.Struct {
		panic("HandleHttpRpc: Delegate output must be a struct")
	}
	
	params := make([]reflect.Value, 0)

	return func(resp http.ResponseWriter, req *http.Request) {
		encoder := json.NewEncoder(resp)
		
		respObjectValue := reflect.ValueOf(rpcDelegate).Call(params)[0]

		resp.Header().Set("Content-Type", "application/json")
		err := encoder.Encode(respObjectValue.Interface())
		if err != nil {
			panic(err)
		}
	}
}
