package httpRpc

import (
	"net/http"
	"log"
)

func HandleErrorsAnd(httpHandler func(resp http.ResponseWriter, req *http.Request)) func(resp http.ResponseWriter, req *http.Request) {
	return func(resp http.ResponseWriter, req *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				log.Println("[ERROR]", err, *req)
	            http.Error(resp, "Internal server error", http.StatusInternalServerError)
	        }
		}()

		httpHandler(resp, req)
	}
}
