package httpRpc

import (
	"net/http"
	"encoding/json"
	"reflect"
)

func HandleHttpPost(rpcDelegate interface{}) func(resp http.ResponseWriter, req *http.Request) {
	delegateType := reflect.TypeOf(rpcDelegate)
	if delegateType.NumIn() != 1 {
		panic("HandleHttpRpc: Delegate should have exactly one input")
	}
	if delegateType.NumOut() != 1 {
		panic("HandleHttpRpc: Delegate should have exactly one output")
	}
	if delegateType.In(0).Kind() != reflect.Ptr || delegateType.In(0).Elem().Kind() != reflect.Struct {
		panic("HandleHttpRpc: Delegate input must be a pointer to a struct")
	}
	if delegateType.Out(0).Kind() != reflect.Struct {
		panic("HandleHttpRpc: Delegate output must be a struct")
	}
	
	reqType := delegateType.In(0).Elem()
	params := make([]reflect.Value, 1)

	return func(resp http.ResponseWriter, req *http.Request) {
		decoder := json.NewDecoder(req.Body)
		encoder := json.NewEncoder(resp)
		
		reqObjectValue := reflect.New(reqType)
		err := decoder.Decode(reqObjectValue.Interface())
		if err != nil {
			http.Error(resp, "Bad request", http.StatusBadRequest)
			return
		}

		params[0] = reqObjectValue
		respObjectValue := reflect.ValueOf(rpcDelegate).Call(params)[0]

		resp.Header().Set("Content-Type", "application/json")
		err = encoder.Encode(respObjectValue.Interface())
		if err != nil {
			panic(err)
		}
	}
}
